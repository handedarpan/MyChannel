package com.org.MyPackage.MyChannel;

import java.net.MalformedURLException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.Utility.Browser;

public class DarpanHande {
	public static WebDriver driver;
	@BeforeClass
	@Parameters({ "browser" })
	public void setUp(String browser) throws MalformedURLException {
		System.out.println("*******************");
		driver = Browser.getDriver(browser);
		driver.manage().window().maximize();
	}

	@Test
	public void link1() throws InterruptedException {
		//Enter URL
		driver.get("https://www.youtube.com/watch?v=SIfJDP2eHYY");
		WebDriverWait wait=new WebDriverWait(driver, 30);
		Thread.sleep(10000);

	}

	@Test
	public void link2() throws InterruptedException {
		//Enter URL
		driver.get("https://www.youtube.com/watch?v=EKw0VAzpSBc");
		WebDriverWait wait=new WebDriverWait(driver, 30);
		Thread.sleep(10000);

	}

	@Test
	public void link3() throws InterruptedException {
		//Enter URL
		driver.get("https://www.youtube.com/watch?v=C2VUG63FJRk");
		WebDriverWait wait=new WebDriverWait(driver, 30);
		Thread.sleep(10000);

	}

	@Test
	public void link4() throws InterruptedException {
		//Enter URL
		driver.get("https://www.youtube.com/watch?v=1uV35zEdC9g");
		WebDriverWait wait=new WebDriverWait(driver, 30);
		Thread.sleep(10000);

	}

	@Test
	public void link5() throws InterruptedException {
		//Enter URL
		driver.get("https://www.youtube.com/watch?v=iMWp3V_2pQY");
		WebDriverWait wait=new WebDriverWait(driver, 30);
		Thread.sleep(10000);

	}

	@Test
	public void link6() throws InterruptedException {
		//Enter URL
		driver.get("https://www.youtube.com/watch?v=itqaIKvJb3M");
		WebDriverWait wait=new WebDriverWait(driver, 30);
		Thread.sleep(10000);

	}
	
	@Test
	public void link7() throws InterruptedException {
		//Enter URL
		driver.get("https://www.youtube.com/watch?v=OvSZAvmbGHE");
		WebDriverWait wait=new WebDriverWait(driver, 30);
		Thread.sleep(10000);

	}

	/*@BeforeMethod
	public void beforeMethod() {

		driver.get("https://www.youtube.com/");

	}*/

	@AfterClass
	public void afterMethod() {
		driver.quit();
	}

}
