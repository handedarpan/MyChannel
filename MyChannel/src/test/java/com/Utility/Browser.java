package com.Utility;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class Browser {
	
	public static WebDriver driver;
	
	public static WebDriver getDriver(String browser) {
		switch (browser) {
		case "firefox":
			System.out.println("Opening firefox driver");
			System.setProperty("webdriver.gecko.driver", "C:\\Users\\hp\\eclipse-workspace\\MyChannel\\lib\\geckodriver.exe");
			
			driver=new FirefoxDriver();
			return driver;
		case "chrome":
			System.out.println("Opening chrome driver");
			System.setProperty("webdriver.chrome.driver", "C:\\Users\\hp\\eclipse-workspace\\MyChannel\\lib\\chromedriver.exe");
			
			driver=new ChromeDriver();
			return driver;
		case "IE":
			System.out.println("Opening IE driver");
			//return DesiredCapabilities.internetExplorer();
			driver=new InternetExplorerDriver();
			return driver;
		default:
			System.out.println("browser : " + browser + " is invalid, Launching Firefox as browser of choice..");
			//return DesiredCapabilities.firefox();
			System.setProperty("webdriver.gecko.driver", "C:\\Users\\hp\\eclipse-workspace\\MyChannel\\lib\\geckodriver.exe");
			driver=new FirefoxDriver();
			return driver;
		}
	}
	
	/*public static RemoteWebDriver getDriver(String browser) throws MalformedURLException {
		return new RemoteWebDriver(new URL("http://192.168.85.1:4444/wd/hub"), getBrowserCapabilities(browser));
	}*/
	
	private static DesiredCapabilities getBrowserCapabilities(String browserType) {
		switch (browserType) {
		case "firefox":
			System.out.println("Opening firefox driver");
			//System.setProperty("webdriver.gecko.driver", "C:\\Users\\hp\\eclipse-workspace\\TestNGDemo\\lib\\geckodriver\\geckodriver.exe");
			return DesiredCapabilities.firefox();
		case "chrome":
			System.out.println("Opening chrome driver");
			//System.setProperty("webdriver.chrome.driver", "C:\\Users\\hp\\eclipse-workspace\\TestNGDemo\\lib\\chromedriver\\chromedriver.exe");
			return DesiredCapabilities.chrome();
		case "IE":
			System.out.println("Opening IE driver");
			return DesiredCapabilities.internetExplorer();
		default:
			System.out.println("browser : " + browserType + " is invalid, Launching Firefox as browser of choice..");
			return DesiredCapabilities.firefox();
		}
	}

}
